package main

import "github.com/StackExchange/wmi"

// Win32_ComputerSystem is a subset of the Win32_ComputerSystem WMI class
type Win32_ComputerSystem struct {
	Name, UserName string
}

// GetComputerInfo returns the computer name and the currently logged on user
func getComputerInfo(info *ComputerInfo) (err error) {
	var result []Win32_ComputerSystem
	query := wmi.CreateQuery(&result, "")
	if err = wmi.Query(query, &result); err != nil {
		return err
	}

	info.Name = result[0].Name
	info.CurrentUser = result[0].UserName
	return
}
