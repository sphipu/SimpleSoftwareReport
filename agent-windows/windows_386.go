// +build windows,386

package main

import (
	"syscall"
	"unsafe"
)

// is64BitOS returns if the system is 64 bit
// the 32 bit executable may be executed on windows 64 bit with
// WoW64, we have to check that
func is64BitOS() (bool, error) {
	dll, err := syscall.LoadDLL("kernel32.dll")
	if err != nil {
		return false, err
	}
	defer dll.Release()

	proc, err := dll.FindProc("IsWow64Process")
	if err != nil {
		return false, err
	}

	handle, err := syscall.GetCurrentProcess()
	if err != nil {
		return false, err
	}
	var result bool

	v, _, err := proc.Call(uintptr(handle), uintptr(unsafe.Pointer(&result)))
	if v == 0 {
		// There was an error. See https://docs.microsoft.com/en-us/windows/desktop/api/wow64apiset/nf-wow64apiset-iswow64process2
		return false, err
	}

	return result, nil
}
