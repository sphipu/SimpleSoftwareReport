package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
)

func sendReport(report *Report, server string) (err error) {
	b, err := json.Marshal(report)
	if err != nil {
		return
	}
	r := bytes.NewReader(b)
	req, err := http.NewRequest("POST", fmt.Sprintf("%s/api/1.0/save_report", server), r)
	if err != nil {
		return err
	}
	req.Header.Add("Content-Type", "application/json")
	client := http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return err
	}
	if resp.StatusCode != 200 && resp.StatusCode != 204 {
		// Expect OK or NoContent as status
		return fmt.Errorf("Server returned unexpected status code %d", resp.StatusCode)
	}
	return nil
}

func writeReport(report *Report, writer io.Writer) (err error) {
	_, err = fmt.Fprintf(writer, "Computer Name: %s\n", report.ComputerInfo.Name)
	if err != nil {
		return
	}
	_, err = fmt.Fprintf(writer, "Current User: %s\n", report.ComputerInfo.CurrentUser)
	if err != nil {
		return
	}
	_, err = fmt.Fprintln(writer, "Installed Software:")
	if err != nil {
		return
	}
	for _, inst := range report.InstalledSoftware {
		_, err = fmt.Fprintf(writer, "DisplayName: %s\n", inst.DisplayName)
		if err != nil {
			return
		}
		_, err = fmt.Fprintf(writer, "DisplayVersion: %s\n", inst.DisplayVersion)
		if err != nil {
			return
		}
		_, err = fmt.Fprintf(writer, "Publisher: %s\n\n", inst.Publisher)
		if err != nil {
			return
		}
	}
	return
}
