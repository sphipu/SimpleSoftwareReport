from urllib.parse import quote_plus
from datetime import datetime

def quote_plus_filter(s):
    if isinstance(s, str):
        s = s.encode()
    print(s, type(s))
    return quote_plus(s)


def age_filter(moment):
    seconds = (datetime.utcnow() - moment).total_seconds()

    if seconds < 60:
        age = 'less than a'
        entity = 'minute'
    elif seconds < 3600:
        age = round(seconds / 60)
        if age <= 1:
            entity = 'minute'
        else:
            entity = 'minutes'

    elif seconds < 86400:
        age = round(seconds / 3600)
        if age <= 1:
            entity = 'hour'
        else:
            entity = 'hours'

    else:
        import math
        math.floor()
        age = round(seconds / 86400)
        if age <= 1:
            entity = 'day'
        else:
            entity = 'days'

    return '%s %s ago' % (age, entity)
