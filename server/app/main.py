from flask import Flask

from .views import bp as views_bp
from . import database
from . import filters
from .assets import asset_url, asset_url_debug
from .setup.configure import configure
from .setup.init_db import init_db

def create_app():
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_object('app.defaultconfig')
    app.config.from_pyfile('config.py', silent=True)

    app.register_blueprint(views_bp)
    app.teardown_appcontext(lambda e: database.close_connection())

    app.jinja_env.filters['quote_plus'] = filters.quote_plus_filter #pylint: disable=no-member
    app.jinja_env.filters['age'] = filters.age_filter  #pylint: disable=no-member
    if app.debug:
        app.jinja_env.globals['asset_url'] = asset_url_debug #pylint: disable=no-member
    else:
        app.jinja_env.globals['asset_url'] = asset_url #pylint: disable=no-member

    app.cli.command()(configure)
    app.cli.command('init-db')(init_db)
    return app
