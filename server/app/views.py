from marshmallow import ValidationError
from flask import Blueprint, request, abort, render_template, redirect, url_for

from .reports import REPORT_SCHEMA
from .search import search_db
from . import database

bp = Blueprint('views', __name__)

@bp.route('/api/1.0/save_report', methods=['POST'])
def save_report():
    conn = database.get_connection()
    try:
        report = REPORT_SCHEMA.load(request.json)
        print(report)
    except ValidationError:
        abort(400)
    else:
        report.save(conn)
        conn.commit()
    
    return '', 204


@bp.route('/')
def index():
    return render_template('index.html')


@bp.route('/search')
def search():
    query_str = request.args['q']
    if not query_str:
        return redirect(url_for('views.index'))
    try:
        sort_key = int(request.args.get('s', 0))
        reverse = int(request.args.get('r', 0))
    except ValueError:
        abort(400)
    table = search_db(database.get_connection(), query_str, sort_key, reverse)
    return render_template('search.html', **locals())