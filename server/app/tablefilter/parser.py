"""
Parse a tablefilter query. 

SYNTAX
WORD = {A-Za-z0-9}+|"{A-Za-z0-9' }*"|'{A-Za-z0-9" }*'
WORD_EXPRESSION: OPTION* WORD ['IN' COLUMN_NAME]
COLUMN_NAME: WORD
OPTION: 'WORD'|'CASE'
BRACKET: (QUERY)
EXPRESSION: ['NOT'] BRACKET|WORD_EXPRESSION
QUERY: EXPRESSION [('AND'|'OR') EXPRESSION]*
"""

from .tokenizer import tokenize, TokenType
from .exceptions import QuerySyntaxError

class _AndExpression:
    def __init__(self, left, right):
        self.left = left
        self.right = right
    
    def evaluate(self, row):
        return self.left.evaluate(row) and self.right.evaluate(row)

class _OrExpression:
    def __init__(self, left, right):
        self.left = left
        self.right = right
    
    def evaluate(self, row):
        return self.left.evaluate(row) or self.right.evaluate(row)


class _WordExpression:
    def __init__(self, word, ignore_case, whole_word, column):
        self.word = word.upper() if ignore_case else word
        self.ignore_case = ignore_case
        self.whole_word = whole_word
        self.column = column
    
    def _evaluate_cell(self, word):
        word = str(word)
        if self.ignore_case:
            word = word.upper()
        return self.word == word if self.whole_word else self.word in word
    

    def _evaluate_row(self, row):
        for cell in row:
            if self._evaluate_cell(cell):
                return True
        return False

    
    def evaluate(self, row):
        if self.column is None:
            matches = self._evaluate_row(row)
        else:
            try:
                cell = row[self.column]
            except IndexError:
                matches = False
            else:
                matches = self._evaluate_cell(cell)
        return matches



class _NotExpression:
    def __init__(self, expr):
        self.expr = expr
    
    def evaluate(self, row):
        return not self.expr.evaluate(row)


class _Parser:
    def __init__(self, query_str, column_names):
        self.column_names = column_names
        self.token_gen = tokenize(query_str)
        self.token = next(self.token_gen)
    
    def read_word(self):
        ignore_case = True
        whole_word = False
        column = None

        # read options
        while self.token.type != TokenType.WORD:
            if self.token.type == TokenType.WHOLE_WORD and not whole_word:
                whole_word = True
            elif self.token.type == TokenType.RESPECT_CASE and ignore_case:
                ignore_case = False
            else:
                raise self.syntax_error()
            self.token = next(self.token_gen)
        
        word = self.token.value
        self.token = next(self.token_gen)

        if self.token.type == TokenType.IN:
            self.token = next(self.token_gen)
            try:
                column = self.column_names[self.token.value]
            except KeyError:
                try:
                    column = int(self.token.value) - 1
                    if column < 0:
                        raise ValueError
                except ValueError:
                    raise QuerySyntaxError("Invalid column specifier %s" % self.token.value)
            self.token = next(self.token_gen)

        return _WordExpression(word, ignore_case, whole_word, column)
    
        
    def syntax_error(self):
        message = 'Invalid token %s at %d' % (self.token.text, self.token.position)
        return QuerySyntaxError(message)

    def read_expression(self):
        if self.token.type == TokenType.NOT:
            self.token = next(self.token_gen)
            if self.token.type == TokenType.OPEN_BRACKET:
                self.token = next(self.token_gen)
                expr = _NotExpression(self.read_query(TokenType.CLOSE_BRACKET))
            else:
                expr = _NotExpression(self.read_word())
        elif self.token.type == TokenType.OPEN_BRACKET:
            self.token = next(self.token_gen)
            expr = self.read_query(TokenType.CLOSE_BRACKET)
        else:
            expr = self.read_word()
        return expr

    def read_query(self, end):
        expr = self.read_expression()

        while self.token.type != end:
            if self.token.type == TokenType.AND:
                self.token = next(self.token_gen)
                expr = _AndExpression(expr, self.read_expression())
            elif self.token.type == TokenType.OR:
                self.token = next(self.token_gen)
                expr = _OrExpression(expr, self.read_expression())
            else:
                raise self.syntax_error()
        return expr


def parse(query_str, column_names):
    parser = _Parser(query_str, column_names)
    try:
        return parser.read_query(TokenType.EOF)
    except StopIteration:
        raise parser.syntax_error()
    
         
    