import pymysqlpool
import pymysql
from flask import g, current_app


def _get_db_config(config):
    db_config = {
        'user': config['DB_USER'],
        'password': config['DB_PASSWORD'],
        'db': config['DB_NAME'],
        'charset': config['DB_CHARSET'],
        'port': config['DB_PORT']
    }
    return db_config


def new_pool(config):
    db_config = _get_db_config(config)
    pool = pymysqlpool.ConnectionPool(config['DB_POOL_SIZE'], **db_config)
    return pool


def new_connection(config):
    db_config = _get_db_config(config)
    conn = pymysql.connect(**db_config)
    return conn


def get_pool():
    try:
        return current_app._db_pool
    except AttributeError:
        current_app._db_pool = new_pool(current_app.config)
        return current_app._db_pool


def get_connection():
    try:
        conn = g._db_connection
    except AttributeError:
        conn = get_pool().get_connection()
        g._db_connection = conn
    return conn 


def close_connection():
    try:
        g._db_connection.close()
    except AttributeError:
        pass
